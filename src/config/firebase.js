const config = {
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  // Only needed if using Firebase Realtime Database
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  // Only needed if using Firebase Authentication
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  // Only needed if using Firebase Storage
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  // What's this?
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
};

export default config;
