import Rebase from 're-base';
import firebase from 'firebase';

import config from '../config/firebase';

const app = firebase.initializeApp(config);
const Base = Rebase.createClass(app.database());

export default Base;
