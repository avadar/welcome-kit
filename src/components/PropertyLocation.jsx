import React from 'react';
import styled from 'styled-components';

const Root = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-bottom: 1.5rem;
  border-bottom: 1px solid #AAA;
`;
const AddressContainer = styled.div`
  flex: 1 1 auto;
  align-self: stretch;
  display: flex;
  flex-direction: column;
`;
const Address = styled.div`
  flex: 1 1 auto;
`;
const Text = styled.p`
  margin: 0;
`;
const ActionLinkContainer = styled.div`
  flex: 0 0 auto;
`;
const ActionLink = styled.a`
  cursor: pointer;
  text-decoration: underline;
  &:not(:first-of-type) {
    margin-left: 1rem;
  }
`;
const MinimapContainer = styled.div`
  flex: 0 0 auto;
  margin-left: 1rem;
`;
const Minimap = styled.div`
  width: 72px;
  height: 72px;
  background-color: #AAA;
  border-radius: 12px;
`;

const PropertyLocation = ({ street1, street2, city, state, zip }) => (
  <Root>
    <AddressContainer>
      <Address>
        <Text>{street1}</Text>
        <Text>{street2}</Text>
        <Text>{`${city}, ${state} ${zip}`}</Text>
      </Address>
      <ActionLinkContainer>
        <ActionLink href="#">Directions</ActionLink>
        <ActionLink href="#">Open in Maps</ActionLink>
      </ActionLinkContainer>
    </AddressContainer>
    <MinimapContainer>
      <Minimap />
    </MinimapContainer>
  </Root>
);

export default PropertyLocation;
