import React from 'react';
import styled from 'styled-components';

const Root = styled.div`
  margin-bottom: 2rem;
`;
const Header = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
const Body = styled.div`
  max-height: ${({collapsed, maxHeight}) => collapsed ? '0' : maxHeight};
  transition: max-height 200ms ease-in-out;
  overflow: hidden;
`;
const Title = styled.h3`
  flex: 1 1 auto;
  margin: 0;
`;
const ButtonLink = styled.a`
  cursor: pointer;
  font-size: 13px;
  text-decoration: underline;
`;

class Collapsible extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      collapsed: (props && props.collapsed) || false,
      maxHeight: undefined,
    };
    this.onButtonLinkClicked = this.onButtonLinkClicked.bind(this);
    this.handleBodyRef = this.handleBodyRef.bind(this);
  }

  toggleCollapsed() {
    this.setState({ collapsed: !this.state.collapsed });
  }

  setMaxHeight(value) {
    this.setState({ maxHeight: `${value}px` });
  }

  handleBodyRef(node) {
    // Is ran affter element is mounted?
    this.setMaxHeight(node.clientHeight);
  }

  onButtonLinkClicked(event) {
    event.preventDefault();
    this.toggleCollapsed();
  }

  render() {
    let { title, children, collapseText = 'Collapse', expandText = 'Expand' } = this.props;
    let { collapsed, maxHeight } = this.state;

    return (
      <Root>
        <Header>
          <Title>{title}</Title>
          <ButtonLink onClick={this.onButtonLinkClicked}>
            {collapsed ? expandText : collapseText}
          </ButtonLink>
        </Header>
        <Body innerRef={this.handleBodyRef} collapsed={collapsed} maxHeight={maxHeight}>
          {this.props.children && React.Children.only(children)}
        </Body>
      </Root>
    );
  }

};

export default Collapsible;
