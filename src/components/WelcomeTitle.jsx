import React from 'react';
import styled from 'styled-components';

const Header = styled.h1`
  text-align: center;
  margin-bottom: 2rem;
`;

const WelcomeTitle = ({ title }) => (
  <Header>{title}</Header>
);

export default WelcomeTitle;
