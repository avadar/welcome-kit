import React from 'react';
import Styled from 'styled-components';

const Root = Styled.div`
  display: flex;
  flex-direction: row;
  &:not(:last-of-type) {
    margin-bottom: 1.5rem;
  }
`;
const ImageContainer = Styled.div`
  flex: 0 0 auto;
`;
const Image = Styled.img`
  width: 64px;
  height: 64px;
  border-radius: 12px;
  background-color: #AAA;
`;
const InfoContainer = Styled.div`
  flex: 1 1 auto;
  margin-left: 1rem;
`;
const Name = Styled.h3`
  margin-top: 0;
  margin-bottom: 1rem;
`;
const Tips = Styled.p``;
const Row = Styled.div`
  display: flex;
  flex-direction: row;
`;
const Location = Styled.span`
  flex: 1 1 auto;
  font-style: italic;
`;
const ActionLink = Styled.a`
  flex: 0 0 auto;
  cursor: pointer;
  margin-left: 1rem;
`;

const calculateDistance = ({ lat, long }) => {
  return `${'??'} Miles away`;
};

const Recommendation = ({ info }) => {
  let { name, pic, tips, location } = info;
  return (
    <Root>
      <ImageContainer>
        <Image src={pic} />
      </ImageContainer>
      <InfoContainer>
        <Name>{name}</Name>
        <Tips>{tips}</Tips>
        <Row>
          <Location>{calculateDistance(location)}</Location>
          <ActionLink href="#">Open in Maps</ActionLink>
        </Row>
      </InfoContainer>
    </Root>
  );
};

export default Recommendation;
