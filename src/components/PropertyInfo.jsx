import React from 'react';
import styled from 'styled-components';

const Root = styled.div`
  padding-top: 1.5rem;
`;
const Row = styled.div`
  display: flex;
  flex-direction: row;
`;
const InfoName = styled.div`
  flex: 1 1 50%;
  font-weight: bold;
`;
const InfoValue = styled.div`
  flex: 1 1 50%;
`;

const PropertyInfo = ({ info }) => (
  <Root>
    {info && info.map((entry, index) => {
      return (
        <Row key={index}>
          <InfoName>{entry.name}</InfoName>
          <InfoValue>{entry.value}</InfoValue>
        </Row>
      );
    })}
  </Root>
);

export default PropertyInfo;
