import React from 'react';
import styled from 'styled-components';
import PropertyLocation from './PropertyLocation';
import PropertyInfo from './PropertyInfo';

const Root = styled.div`
  margin-bottom: 2rem;
`;

const Title = styled.h3`

`;

const PropertyOverview = ({ location, info }) => (
  <Root>
    <Title>Property Overview</Title>
    <PropertyLocation {...location} />
    <PropertyInfo info={info} />
  </Root>
);

export default PropertyOverview;
