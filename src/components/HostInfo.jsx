import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-bottom: 2rem;
`;

const ImageContainer = styled.div`
  flex: 0 0 auto;
  display: flex;
  align-items: center;
`;

const Image = styled.img`
  height: 64px;
  width: 64px;
  background-color: #AAA;
  border-radius: 12px;
`;

const InfoContainer = styled.div`
  flex: 0 0 auto;
  margin-left: 1rem;
`;

const Title = styled.h3`
  margin-top: 0;
  margin-bottom: 0.5rem;
`;

const ActionLink = styled.a`
  display: block;
  margin-top: 0;
  margin-bottom: 0;
  cursor: pointer;
`;

const HostInfo = ({ pic, fullName, phone, email }) => (
  <Container>
    <ImageContainer>
      <Image src={pic} />
    </ImageContainer>
    <InfoContainer>
      <Title>{fullName}</Title>
      <ActionLink href="#">{phone}</ActionLink>
      <ActionLink href="#">{email}</ActionLink>
    </InfoContainer>
  </Container>
);

export default HostInfo;
