import React from 'react';
import styled from 'styled-components';

const Root = styled.div`
  margin-bottom: 2rem;
`;

const WelcomeMessage = ({ children }) => (
  <Root>
    {children}
  </Root>
);

export default WelcomeMessage;
