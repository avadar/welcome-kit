import React from 'react';
import styled from 'styled-components';

import Recommendation from './Recommendation';

const Root = styled.ul`
  padding-left: 0;
`;

const Recommendations = ({ list }) => (
  <Root>
    {list && list.map((recommendation, index) => (
      <Recommendation key={index} info={recommendation} />)
    )}
  </Root>
);

export default Recommendations;
