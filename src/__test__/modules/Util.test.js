import { objIsEmpty } from '../../modules/Util';

describe('objIsEmpty', () => {
  it('should be importable', () => {
    expect(objIsEmpty).toBeDefined();
  });

  it('should be a function', () => {
    expect(typeof objIsEmpty).toBe('function');
  });

  it('should return true if passed an empty object', () => {
    expect(objIsEmpty({})).toBe(true);
  });

  it('should return false if passed a non-empty object', () => {
    expect(objIsEmpty({ key: 'value' })).toBe(false);
  });
});
