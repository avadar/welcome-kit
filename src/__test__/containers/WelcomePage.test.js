import React from 'react';
import { mount } from 'enzyme';
import WelcomePage from '../../containers/WelcomePage';

jest.mock('../../modules/Base', () => {
  return {};
});

describe('WelcomePage', () => {

  it('is importable', () => {
    expect(WelcomePage).toBeDefined();
  });

  describe('when mounted', () => {

    const match = { params: { propertyId: 'propertyId' }};

    const hostAndPropertyIds = Promise.resolve({ hostId: '000', propertyId: '000' });
    const host = Promise.resolve({ host: 'host' });
    const property = Promise.resolve({ property: 'property' });

    WelcomePage.prototype.fetchHostAndPropertyIds = jest.fn(() => hostAndPropertyIds);
    WelcomePage.prototype.fetchHostInfo = jest.fn(() => host);
    WelcomePage.prototype.fetchPropertyInfo = jest.fn(() => property);

    it('doesn\'t crash', () => {
      mount(<WelcomePage match={match} />);
    });

    it('sets needed state', async done => {
      const wrapper = mount(<WelcomePage match={match} />);
      await Promise.all([ hostAndPropertyIds, host, property ]);

      setImmediate(() => { makeAssertion(); });

      const makeAssertion = () => {
        // https://github.com/facebook/jest/issues/2059 should make this try catch to be a part of
        // the framework.
        try {
          expect(wrapper.state()).toEqual({
            hostId: '000',
            host: { host: 'host' },
            propertyId: '000',
            property: { property: 'property' }
          });
        } catch (error) {
          done.fail(error);
        }
        done();
      };
    });

  });

});
