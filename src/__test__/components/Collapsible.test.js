import React from 'react';
import { mount, shallow } from 'enzyme';
import Collapsible from '../../components/Collapsible';

describe('Collapsible', () => {

  it('is importable', () => {
    expect(Collapsible).toBeDefined();
  });

  it('renders without crash', () => {
    mount(<Collapsible />);
  });

  it('has a boolean collapsed state', () => {
    const el = new Collapsible();
    expect(el.state.collapsed).toBeDefined();
    expect(typeof el.state.collapsed).toBe('boolean');
  });

  it('defaults its collapsed state to false', () => {
    const el = new Collapsible();
    expect(el.state.collapsed).toBe(false);
  });

  it('sets its collapsed state to the passed props.collapsed', () => {
    const el = new Collapsible({ collapsed: true });
    expect(el.state.collapsed).toBe(true);
  });

  it('is able to toggle its collapsed state', async () => {
    expect.assertions(2);
    const wrapper = mount(<Collapsible />);
    wrapper.instance().toggleCollapsed();
    await expect(wrapper.instance().state.collapsed).toBe(true);
    wrapper.instance().toggleCollapsed();
    await expect(wrapper.instance().state.collapsed).toBe(false);
  });

  it('renders the only child', () => {
    const child = <div className="anything"></div>;
    const wrapper = mount(<Collapsible>{child}</Collapsible>);
    expect(wrapper.contains(child)).toBe(true);
  });

});
