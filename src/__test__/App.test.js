import React from 'react';
import ReactDOM from 'react-dom';
import App from '../containers/App';

// Stub this out until we can mock the Base module
jest.mock('../containers/WelcomePage', () => null);

xdescribe('App', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
  });
});
