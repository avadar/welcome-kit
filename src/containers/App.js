import React, { Component } from 'react';
import styled from 'styled-components';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Default from '../components/Default';
import WelcomePage from '../containers/WelcomePage';
import '../styles/App.css';

const Root = styled.div`
  max-width: 760px;
  min-width: 320px;
  margin: 0 auto;
  padding: 1rem;
`;

class App extends Component {
  render() {
    return (
      <Root>
        <Router>
          <Switch>
            <Route exact path='/' component={Default} />
            <Route path='/:propertyId' component={WelcomePage} />
          </Switch>
        </Router>
      </Root>
    );
  }
}

export default App;
