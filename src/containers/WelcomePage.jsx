import React from 'react';
import styled from 'styled-components';
import WelcomeTitle from '../components/WelcomeTitle';
import HostInfo from '../components/HostInfo';
import WelcomeMessage from '../components/WelcomeMessage';
import PropertyOverview from '../components/PropertyOverview';
import Recommendations from '../components/Recommendations';
import Collapsible from '../components/Collapsible';
import Base from '../modules/Base';
import { objIsEmpty } from '../modules/Util';

const StyledWelcomePage = styled.div`

`;

class WelcomePage extends React.Component {

  state = {
    hostId: undefined,
    host: {},
    propertyId: undefined,
    property: {}
  };

  defaultContext = { context: this };

  fetchHostAndPropertyIds(routeParam) {
    return Base
      .fetch(`routes/${routeParam}`, this.defaultContext)
      .catch(err => console.log('Error in fetching host and property id\'s', err));
  }

  fetchHostInfo(hostId) {
    return Base
      .fetch(`hosts/${hostId}`, this.defaultContext)
      .catch(err => console.log('Error in fetching host info', err));
  }

  fetchPropertyInfo(propertyId) {
    return Base
      .fetch(`properties/${propertyId}`, this.defaultContext)
      .catch(err => console.log('Error in fetching property info', err));
  }

  async componentDidMount() {
    let routeParam = this.props.match.params.propertyId;
    const { hostId, propertyId } = await this.fetchHostAndPropertyIds(routeParam);
    this.setState({ hostId, propertyId });
  }

  async componentDidUpdate(prevProps, prevState) {
    let hostAndPropertyIdsAreUpdated = this.state.hostId !== prevState.hostId &&
      this.state.propertyId !== prevState.propertyId;

    if (hostAndPropertyIdsAreUpdated) {
      const [ host, property ] = await Promise.all([
        this.fetchHostInfo(this.state.hostId),
        this.fetchPropertyInfo(this.state.propertyId)
      ]);
      this.setState({ host, property });
    }
  }

  render() {
    let {
      host: {
        fullName,
        phone,
        email,
        pic
      },
      property: {
        welcomeTitle,
        welcomeMessage,
        location,
        info,
        houseRules,
        checkInProcedures,
        historyOfArvada,
        recommendations,
        checkOutProcedures,
      }
    } = this.state;

    if (objIsEmpty(this.state.host) && objIsEmpty(this.state.property)) {
      return null;
    }

    return (
      <StyledWelcomePage>
        <WelcomeTitle title={welcomeTitle} />
        <HostInfo fullName={fullName} phone={phone} email={email} pic={pic} />
        <WelcomeMessage>{welcomeMessage}</WelcomeMessage>

        <PropertyOverview location={location} info={info} />

        <Collapsible title="House Rules">
          <p dangerouslySetInnerHTML={{__html: houseRules}}></p>
        </Collapsible>

        <Collapsible title="Check-in Procedures">
          <p dangerouslySetInnerHTML={{__html: checkInProcedures}}></p>
        </Collapsible>

        <Collapsible title="Recommendations">
          <Recommendations list={recommendations} />
        </Collapsible>

        <Collapsible title="History of Arvada">
          <p dangerouslySetInnerHTML={{__html: historyOfArvada}}></p>
        </Collapsible>

        <Collapsible title="Check-out Procedures">
          <p dangerouslySetInnerHTML={{__html: checkOutProcedures}}></p>
        </Collapsible>

      </StyledWelcomePage>
    );
  }
}

export default WelcomePage;
